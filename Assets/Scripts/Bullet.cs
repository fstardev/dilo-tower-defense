using UnityEngine;

public class Bullet : MonoBehaviour
{
    private int m_bulletPower;
    private float m_bulletSpeed;
    private float m_bulletSplashRadius;
    

    private Enemy m_targetEnemy;

    private void FixedUpdate()
    {
        if (LevelManager.Instance.IsOver) return;
 
        if (m_targetEnemy == null) return;
        if (!m_targetEnemy.gameObject.activeSelf)
        {
            gameObject.SetActive(false);
            m_targetEnemy = null;
            return;
        }
            
        var targetPosition = m_targetEnemy.transform.position;
        var bulletPosition = transform.position;
        bulletPosition = Vector3.MoveTowards(bulletPosition, targetPosition, m_bulletSpeed * Time.deltaTime);
        transform.position = bulletPosition;

        var direction = targetPosition - bulletPosition;
        var targetAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, targetAngle-90f));
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(m_targetEnemy == null) return;

        if (!other.gameObject.Equals(m_targetEnemy.gameObject)) return;
        gameObject.SetActive(false);
        if (m_bulletSplashRadius > 0f)
        {
            LevelManager.Instance.ExplodeAt(transform.position, m_bulletSplashRadius, m_bulletPower);
        }
        else
        {
            m_targetEnemy.ReduceEnemyHealth(m_bulletPower);
        }

        m_targetEnemy = null;
    }
    
    public void SetProperties(int bulletPower, float bulletSpeed, float bulletSplashRadius)
    {
        m_bulletPower = bulletPower;
        m_bulletSpeed = bulletSpeed;
        m_bulletSplashRadius = bulletSplashRadius;
    }

    public void SetTargetEnemy(Enemy enemy)
    {
        m_targetEnemy = enemy;
    }
}
