using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    private static AudioPlayer _instance = null;

    public static AudioPlayer Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<AudioPlayer>();
            }

            return _instance;
        }
    }

    [SerializeField] private List<AudioClip> audioClips;
    private AudioSource m_audioSource;

    private void Awake()
    {
        m_audioSource = GetComponent<AudioSource>();
    }

    public void PlaySfx(string soundName)
    {
        var sfx = audioClips.Find(s => s.name == soundName);
        if(sfx == null) return;
        m_audioSource.PlayOneShot(sfx);
    }
}
