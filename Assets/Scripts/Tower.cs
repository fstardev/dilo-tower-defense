using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [Header("Tower Component")]
    [SerializeField] private SpriteRenderer towerPlace;
    [SerializeField] private SpriteRenderer towerHead;

    [Header("Tower Properties")] 
    [SerializeField] private int shootPower = 1;
    [SerializeField] private float shootDistance = 1f;
    [SerializeField] private float shootDelay = 5f;
    [SerializeField] private float bulletSpeed = 1f;
    [SerializeField] private float bulletSplashRadius;

    [Header("Bullet")] [SerializeField] private Bullet bulletPrefab;

    private float m_runningShootDelay;
    private Enemy m_targetEnemy;
    private Quaternion m_targetRotation;

    public void CheckNearestEnemy(IEnumerable<Enemy> enemies)
    {
        if (m_targetEnemy != null)
        {
            if (!m_targetEnemy.gameObject.activeSelf || Vector3.Distance(transform.position, m_targetEnemy.transform.position) > shootDistance)
            {
                m_targetEnemy = null;
            }
            else
            {
                return;
            }
        }

        var nearestDistance = Mathf.Infinity;
        Enemy nearestEnemy = null;

        foreach (var enemy in enemies)
        {
            var distance = Vector3.Distance(transform.position, enemy.transform.position);
            if (distance > shootDistance)
            {
                continue;
            }

            if (!(distance < nearestDistance)) continue;
            nearestDistance = distance;
            nearestEnemy = enemy;
        }

        m_targetEnemy = nearestEnemy;
    }

    public void ShootTarget()
    {
        if(m_targetEnemy == null) return;
        m_runningShootDelay -= Time.unscaledDeltaTime;
        if (!(m_runningShootDelay <= 0f)) return;
        var headHasAimed = Mathf.Abs(towerHead.transform.rotation.eulerAngles.z - m_targetRotation.eulerAngles.z) <
                           10f;
        if (!headHasAimed)
        {
            return;
        }

        var bullet = LevelManager.Instance.GetBulletFromPool(bulletPrefab);
        bullet.transform.position = transform.position;
        bullet.SetProperties(shootPower, bulletSpeed, bulletSplashRadius);
        bullet.SetTargetEnemy(m_targetEnemy);
        bullet.gameObject.SetActive(true);
        m_runningShootDelay = shootDelay;
    }

    public void SeekTarget()
    {
        if (m_targetEnemy == null) return;
        var direction = m_targetEnemy.transform.position - transform.position;
        var targetAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        m_targetRotation = quaternion.Euler(new Vector3(0, 0, targetAngle - 90f));

        towerHead.transform.rotation =
            Quaternion.RotateTowards(towerHead.transform.rotation, m_targetRotation, Time.deltaTime * 180f);
    }

    public Vector2? PlacePosition { get; private set; }
    public Sprite GetTowerHeadIcon()
    {
        return towerHead.sprite;
    }

    public void SetPlacePosition(Vector2? newPosition)
    {
        PlacePosition = newPosition;
    }

    public void LockPlacement()
    {
        if (PlacePosition != null) transform.position = (Vector2) PlacePosition;
    }

    public void ToggleOrderInLayer(bool toFront)
    {
        var orderInLayer = toFront ? 2 : 0;
        towerPlace.sortingOrder = orderInLayer;
        towerHead.sortingOrder = orderInLayer;
    }
}