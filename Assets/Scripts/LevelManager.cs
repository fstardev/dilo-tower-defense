using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{
    
    private static LevelManager _instance = null;

    public static LevelManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<LevelManager>();
            }

            return _instance;
        }
    }

    [Header("Tower")]
    [SerializeField] private Transform towerUIParent;
    [SerializeField] private GameObject towerUIPrefab;
    [SerializeField] private Tower[] towerPrefabs;

    [Header("Enemy")]
    [SerializeField] private Enemy[] enemyPrefabs;
    [SerializeField] private Transform[] enemyPaths;
    [SerializeField] private float spawnDelay = 5f;
    [SerializeField] private int maxLives = 3;
    [SerializeField] private int totalEnemy = 15;

    [Header("UI")]
    [SerializeField] private GameObject panel;
    [SerializeField] private Text statusInfo;
    [SerializeField] private Text livesInfo;
    [SerializeField] private Text totalEnemyInfo;

    private List<Tower> m_spawnedTowers = new List<Tower>();
    private List<Enemy> m_spawnedEnemies = new List<Enemy>();
    private List<Bullet> m_spwanedBullets = new List<Bullet>();
    private float m_runningSpawnDelay;
    private int m_currentLives;
    private int m_enemyCounter;
    
    public bool IsOver { get; private set; }

    private void Start()
    {
        SetCurrentLives(maxLives);
        SetTotalEnemy(totalEnemy);
        InstantiateAllTowerUI();
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        
        if(IsOver) return;
        
        m_runningSpawnDelay -= Time.unscaledDeltaTime;
        if (m_runningSpawnDelay <= 0f)
        {
            SpawnEnemy();
            m_runningSpawnDelay = spawnDelay;
        }

        foreach (var enemy in m_spawnedEnemies)
        {
            if(!enemy.gameObject.activeSelf) continue;
            var distance = Vector2.Distance(enemy.transform.position, enemy.TargetPosition);
            if (distance < 0.1f)
            {
                enemy.SetCurrentPathIndex(enemy.CurrentPathIndex + 1);
               
                if (enemy.CurrentPathIndex < enemyPaths.Length)
                {
                    enemy.SetTargetPosition(enemyPaths[enemy.CurrentPathIndex].position);
                }
                else
                {
                   
                    ReduceLives(1);
                    enemy.gameObject.SetActive(false);
                }
            }
            else
            {
                enemy.MoveToTarget();
            }
        }

        foreach (var tower in m_spawnedTowers)
        {
            tower.CheckNearestEnemy(m_spawnedEnemies);
            tower.SeekTarget();
            tower.ShootTarget();
        }
    }


    private void ReduceLives(int value)
    {
        SetCurrentLives(m_currentLives - value);
        if (m_currentLives <= 0)
        {
            SetGameOver(false);
        }
    }
    
    private void SetTotalEnemy(int total)
    {
        m_enemyCounter = total;
        totalEnemyInfo.text = $"Total Enemy: {Mathf.Max(m_enemyCounter, 0)}";
    }
    private void SetCurrentLives(int value)
    {
        m_currentLives = Mathf.Max(value, 0);
        livesInfo.text = $"Lives: {m_currentLives}";
    }
    
    private void SetGameOver(bool isWin)
    {
        IsOver = true;
        statusInfo.text = isWin ? "You Win!" : "You Lose!";
        panel.gameObject.SetActive(true);
    }

    private void InstantiateAllTowerUI()
    {
        foreach (var tower in towerPrefabs)
        {
            var newTowerUIObj = Instantiate(towerUIPrefab.gameObject, towerUIParent);
            var newTowerUI = newTowerUIObj.GetComponent<TowerUI>();
            newTowerUI.SetTowerPrefab(tower);
            newTowerUI.transform.name = tower.name;
        }
    }

    public void RegisterSpawnedTower(Tower tower)
    {
        m_spawnedTowers.Add(tower);
    }
    
    private void SpawnEnemy()
    {
        SetTotalEnemy(-- m_enemyCounter);
        if (m_enemyCounter < 0)
        {
            var isAllEnemyDestroyed = m_spawnedEnemies.Find(e => e.gameObject.activeSelf) == null;
            if (isAllEnemyDestroyed)
            {
                SetGameOver(true);
            }
            return;
        }
        var randomIndex = Random.Range(0, enemyPrefabs.Length);
        var enemyIndexStr = $"{randomIndex + 1}";

        var newEnemyObj = m_spawnedEnemies.Find(e => !e.gameObject.activeSelf && e.name.Contains(enemyIndexStr))
            ?.gameObject;
        if (newEnemyObj == null)
        {
            newEnemyObj = Instantiate(enemyPrefabs[randomIndex].gameObject);
        }

        var newEnemy = newEnemyObj.GetComponent<Enemy>();
        if (!m_spawnedEnemies.Contains(newEnemy))
        {
            m_spawnedEnemies.Add(newEnemy);
        }

        newEnemy.transform.position = enemyPaths[0].position;
        newEnemy.SetTargetPosition(enemyPaths[1].position);
        newEnemy.SetCurrentPathIndex(1);
        newEnemy.gameObject.SetActive(true);
    }

    private void OnDrawGizmos()
    {
        for (var i = 0; i < enemyPaths.Length-1; i++)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(enemyPaths[i].position, enemyPaths[i+1].position);
        }
    }

    public Bullet GetBulletFromPool(Bullet prefab)
    {
        var newBulletObj = m_spwanedBullets.Find(b => !b.gameObject.activeSelf && b.name.Contains(prefab.name))
            ?.gameObject;
        if (newBulletObj == null)
        {
            newBulletObj = Instantiate(prefab.gameObject);
        }

        var newBullet = newBulletObj.GetComponent<Bullet>();
        if (!m_spwanedBullets.Contains(newBullet))
        {
            m_spwanedBullets.Add(newBullet);
        }

        return newBullet;
    }

    public void ExplodeAt(Vector3 point, float radius, int damage)
    {
        foreach (var enemy in m_spawnedEnemies
            .Where(enemy => enemy.gameObject.activeSelf)
            .Where(enemy => Vector2.Distance(enemy.transform.position, point) <= radius))
        {
            enemy.ReduceEnemyHealth(damage);
        }
    }
}
