using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] private Image towerIcon;

    private Tower m_prefabTower;
    private Tower m_currentSpawnedTower;

    public void SetTowerPrefab(Tower tower)
    {
        m_prefabTower = tower;
        towerIcon.sprite = tower.GetTowerHeadIcon();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        var newTowerObj = Instantiate(m_prefabTower.gameObject);
        m_currentSpawnedTower = newTowerObj.GetComponent<Tower>();
        m_currentSpawnedTower.ToggleOrderInLayer(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        var mainCamera = Camera.main;
        if(mainCamera == null) return;
        var mousePosition = Input.mousePosition;
        mousePosition.z = -mainCamera.transform.position.z;
        var targetPosition = mainCamera.ScreenToWorldPoint(mousePosition);
        m_currentSpawnedTower.transform.position = targetPosition;

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (m_currentSpawnedTower.PlacePosition == null)
        {
            Destroy(m_currentSpawnedTower.gameObject);
        }
        else
        {
            m_currentSpawnedTower.LockPlacement();
            m_currentSpawnedTower.ToggleOrderInLayer(false);
            LevelManager.Instance.RegisterSpawnedTower(m_currentSpawnedTower);
            m_currentSpawnedTower = null;
        }
    }
}
