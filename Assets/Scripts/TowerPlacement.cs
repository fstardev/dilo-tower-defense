using UnityEngine;

public class TowerPlacement : MonoBehaviour
{
    private Tower m_placedTower;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (m_placedTower != null)
        {
            print("tower exist");
            return;
        }

        var tower = other.GetComponent<Tower>();
        if (tower != null)
        {
            tower.SetPlacePosition(transform.position);
            m_placedTower = tower;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (m_placedTower == null) return;
        
        m_placedTower.SetPlacePosition(null);
        m_placedTower = null;
    }
}
