using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Attribute")]
    [SerializeField] private int maxHealth = 1;
    [SerializeField] private float moveSpeed = 1f;

    [Header("Sprite")] [SerializeField] private SpriteRenderer healthBar;
    [SerializeField] private SpriteRenderer healthFill;

    private int m_currentHealth;
    
    public Vector3 TargetPosition { get; private set; }
    public int CurrentPathIndex { get; private set; }
    
    private void OnEnable()
    {
        m_currentHealth = maxHealth;
        healthFill.size = healthBar.size;
    }

    public void MoveToTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, TargetPosition, moveSpeed * Time.deltaTime);
    }

    public void SetTargetPosition(Vector3 targetPosition)
    {
        TargetPosition = targetPosition;
        
        healthBar.transform.parent = null;
        
        var enemyTransform = transform;
        var distance = TargetPosition - enemyTransform.position;
        enemyTransform.rotation = Mathf.Abs (distance.y) > Mathf.Abs (distance.x) ? 
            Quaternion.Euler(distance.y > 0 ? new Vector3 (0f, 0f, 90f) : new Vector3 (0f, 0f, -90f)) : 
            Quaternion.Euler(distance.x > 0 ? new Vector3 (0f, 0f, 0f) : new Vector3 (0f, 0f, 180f));
        var healthBarTransform = healthBar.transform;
        healthBarTransform.parent = enemyTransform;
    }

    public void ReduceEnemyHealth(int damage)
    {
        m_currentHealth -= damage;
        AudioPlayer.Instance.PlaySfx("hit-enemy");
        if (m_currentHealth <= 0)
        {
            gameObject.SetActive(false);
            AudioPlayer.Instance.PlaySfx("enemy-die");
        }

        var percentage = (float) m_currentHealth / maxHealth;
        var size = healthBar.size;
        healthFill.size = new Vector2(percentage * size.x, size.y);
    }

    public void SetCurrentPathIndex(int currentIndex)
    {
        CurrentPathIndex = currentIndex;
    }
}
